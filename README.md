
[[_TOC_]]

# Purpose
## Open source contributors friendliness
This project highlights a method to use Gitlab professionally:
1. **without pushing unnecessary licensing costs on contributors.**<br> 
The following organisation will allow Compomentis to sponsor teams by paying fair licensing to Gitlab on their behalf.<br>
Contributors, who may not be able to afford it, will not have to bear that cost.
1. **without requiring contributors to change their git tooling.**<br>
The following organisation will allow Compomentis to accept collaborations solely based on git mode of operations (for example from the regular git CLI).<br>
Contributors, who may not be authorised to use Gitlab for corporate reasons, will still be able to work with us using their usual approved tooling.

## Open source quality and governance
This design intends to assist in the **delivery of business capabilities software for SMEs** in a quality controlled manner and with a focus on affordable and dependable cybersecurity.

It follows that a number of checks and balances need to be applied in order to go beyond "_Bob is a nice guy and he told me he writes good code_".

This is what we will use Gitlab for.

## A Catalogue of high quality business tools
Business capabilities are organised in a catalogue of:
1. **Components**
<br>Components are general purpose fully functional software parts that fulfill a single general purpose and have value in their own right.
<br>By design, they offer a composition ability.
<br>Maps, Charts, Facilities are examples of general purpose components.
<br>_The Design of components is beyond the scope of this document, it will be documented elsewhere and later._
1. **Products**
<br>Products are assemblies of components taking advantage of the composition abilities of components.
<br>Products are the solution to a business problem.
<br>CosmeticsFormulator and HarvestPlan are examples of products.
<br>_The Design of products is beyond the scope of this document, it will be documented elsewhere and later._

> Marc says: **DESIGNED FOR INDEPENDENCE FIRST**
> <br>**My vision is for containerised software that can run on-premises first.**.
<br>As such, while it is not its prime objective:
> <br>It CAN run online if the user so wants it --> Yes you can host it on your web server.
> <br>It CAN run in the cloud "IAS style" --> Yes you can host it on AWS or Azure or others.
> <br>It CAN run in the cloud "SAAS or PAAS style" if the user so wants it. 
> <br>YOU ARE FREE TO MAKE YOUR OWN IAS, SAAS or PAAS using this, but this is not what we do here.

> Marc says: **See products as cordless drills!**
> <br>The cordless drill is a product made of tree components (a chuck, a motor and a battery)
> <br>My cordless drill will use an small motor, look a certain way and use a two speed button, a direction switch and a press button trigger. <br>Whereas yours may use a larger motor, include speed and torque adjusters and a base and column a lubricant dispenser and water cooling.
> <br>The objective here is not to provide a drill that drills all!
> <br>I like the freedom and power of owning two drills, one light and affordable for general purpose jobs in the house and a bigger water cooled column drill for more demanding ones in the workshop.

# Principles

## Agnostic Containerised software.
Compomentis sustains the project by supplying essential Infrastructure in the form of Hardware, Internet, Software Licenses and support to sustain the **CICDPlant**__ which among other things runs Gitlab Servers and Runners.

All artefacts need to "run anywhere", which drives the choice of languages and technologies.
* **Run anywhere** languages that can be professionally built and packaged at least to the level java is AND with reasonble levels of forward and backward compatibility (so java, scala and nodejs are OK, but Python or C# are not).
* **Containerise** software, with a bias towards images built from supported Linux distributions even when it is not strictly speaking necessary.<br>
There is no presumption on how the container will run, by design the containers can be use anywhere.
* **Freedom** for developers.
Use your own hardware as you see fit, Linux, Windows, Mac...<br>
As long as the resulting containers fit the bill above,
<br> _do what you can, where you are with what you have_.


## Governance and Security in Gitlab Premium
The **platform team** runs of an on-prem Gitlab Premium installation.


For argument's sake we can imagine a team of <br>
1. devops and support engineers, who are responsible for the design, operation and security of the platform <br>
1. AND business capabilities leads who are responsible for the delivery of capabilities through the platform which provides quality and security control over the capabilties.

> Marc Says: The system is primed with only one license for myself for cash management reason and also beacuse it is the start and I have not invited anyone yet :-).<br>
> But it will be scaled up at the earliest opportunity.

The platform team has two high level governance features:

1. Their **ownership of the platform's features**.<br>
       The Team designs, develops, maintains and operates the platform.
1. Their **stewardship of the business capabilities**<br>
       The Team receives delivery of software into the platform from one or more business capability team(s); each with their own gitlab group.

       The platform team sets expectations about 
       1. The standard of quality and security for business capabilities.<br>
        This will be controlled using Gitlab Premium.
       2. **AND** the method of delivery<br>
       This is what we describe here.

## Business capability delivery in Gitlab Free
The platform team controls the Design, development and enforcement business capabilities requirements.

These requirements must be communicated to the cabability teams, who must include them in their **Non Functional requirements** (NFR)

    As we will see in the Implementation Seection, much  governance work will occur, we will hide all this complexity from capability teams developers by providing an easy to follow procedure in ** Gitlab Free**.

# Implementation   
## Gitlab ~~Ultimate~~ Premium Components
This is the complex and pricey part we want to shield developers from.

~~This will be achieved by fronting the Ultimate Gitlab with a Free Gitlab.~~<br>
This will be achieved by supplying a Gitlab Premium Licence to each **capability team lead**; compomentis will incur that cost as their sponsorship of the capability team. 

As mentionned above each capability team will be reflected by a Gitlab Group and the capability team lead will be a maintainer of that group.

> Marc says: That makes capability teams lead the equivallent of "sub-maintainers" in the Linux development project. 
This role name is more fitting because the capability team lead is not "sub" anything, this is an experienced and decicated professional.

### pre-receive server hook
Of particular importance is the need to protect the existence and/or content of .gitlab-ci.yml. 

A generic .gitlab-ci.yml file will be supplied that references a canonical .gitlab-ci.yml for that capability team.<br>
This canonical pipeline is agreed upon between the platform team and the capability team lead. 

Canonical pipelines are defined in the **platform group** in gitlab.<br>
They are owned by the platform team and each capability team lead is defined as a developer in that group.

> Marc says: The capability team lead wears two hats!
> 1. That of a devops contributor for the platform.
> 1. That of a maintainer for the capability team.


ON-PREM SECURITY CONF:<br>
The platform team's owner will set<br>

1. A "pre-receive server hook" to enforce the use of the intended .gitlab-ci.yml in pipelines. 
1. Branch rules to ensure developers do not inadvertently or maliciously replace the generic 

### Control projects from Gitlab ~~Ultimate~~ Premium 

All projects are registered in Gitlab ~~Ultimate~~ Premium by the platform team's Owners.

#### Top level /platform group

The /platform group is:
* Owned and maintained by the platform team lead.
* Developed granularily by capability team leads.

The top level /platform group will have: 
1. A /platform/components subgroup that will have individual projects for each component.<br> Each of these projects will hold the **canonical pipeline** for that component.
1. A /platform/products subgroup that will have individual projects for each product.<br> Each of these projects will hold the **canonical pipeline** for that product.



#### Top level /catalogue group

The /catalogue group is:
* Owned by the platform team lead.
* Maintained granularily by capability team leads.
* Developped granularily by capability team contributors.

The top level /catalogue group will have: 
1. A /catalogue/components subgroup that will have individual projects for each component.<br> Each of these projects will hold the **generic pipeline** for that component.
1. A /catalogue/products subgroup that will have individual projects for each product.<br> Each of these projects will hold the **generic pipeline** for that product.




### Code vs Infrastructure Configuration
1. Code is managed, tested, promoted and tracked using git branches, merge requests and git tags.<br>
Configuration parameters are externalised in a **controlled CMDB**.<br> In other words, configuration is not kept in the code.
1. The deployment state is controlled from the CMDB, which triggers deployment pipelines on the basis of the deployment target (environment and CI) and the version number.  

       A CMDB is required for live operations.
       The CMDB must be interfaced with Gitlab to allow for triggering of deployment pipelines.

    `curl -X POST--fail -F token=${TOKEN} -F ref=${REF_NAME} ${GITLAB_URL}/api/v4/projects/${PROJECT_ID}/trigger/pipeline`

New code pushed to the repository has two effects
1. A build stage builds artefacts.
1. A publish stage publish them to repositories.
The artefacts maturity is denoted by a SNAPSHOT|ALPHA|RELEASE annotation.
1. An **optional** deploy stage deploys to the appropriate environment using the parameters retrieved from the CMDB.

       The CMDB should have a REST API to allows Gitlab to fetch parameters.
       There is a two way cooperation at play here:
       1. The CMDB supplies parameters to Gitlab pipelines
       2. Gitlab executes deployment pipelines on behalf of the CMDB.

### branch names and protected branches on the Ultimate Gitlab
Push rules will be used to control which branches can be created.<br>
And protection rules will be applied.

1. main branch: default, protected<br>
This contains the reference code.<br>
Each successful release is merged here with its version tag.
1. /^dev-/ branches: 
These are created by developers as they take on jobs.<br>
--> On push, they only trigger a SAST-only pipeline, we dont need a full build on each push, but we want to detect security issue as soon as possible.
1. /^-feature-/ branches: protected<br>
These are Created by Maintainers as a result of work plannning.<br>
Developers will signal work completion with a Merge Request from a /^dev-/ to a /^feature-/ branch.<br>
--> On Merge (or force push) by a Maintainer a full build will run, leading to signed SNAPSHOT artefacts publication in binary repositories (maven, docker...)<br>
For those artefacts that are services (or part thereof), the service will be deployed to the DEV Kubernetes Cluster (or equivallent)
1. /^-release-/ branches: protected<br>
A Maintainer creates a /^release-/ branch as a result of release planning.<br>
The new version number is set when the branch is created.<br>
The /^feature-/ branches selected for release are merged into the /^release/ branch. <br>
--> On Merge (or force push) by a Maintainer a full build will run, leading to signed ALPHA artefacts publication in binary repositories (maven, docker...)<br>
For those artefacts that are services (or part thereof), the service will be deployed to the ALPHA Kubernetes Cluster (or equivallent)


